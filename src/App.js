import logo from './logo.svg';
import './App.css';
import NavigatiosBar from './components/NavigatiosBar';
import Intro from './components/Intro';
import Trending from './components/Trending';
import Superhero from './components/Superhero';
import "./style/landingPages.css"
function App() {
  return (
    <div> 
      {/* intro section */}
      <div className="myBg">
      <NavigatiosBar/> 
      <Intro/>
      </div>
      {/* end of intro section */}

    {/* trending */}
    <div className="trending">
      <Trending/>
    </div>
    {/* end of trending */}

    {/* superhero */}
    <div className="superhero">
      <Superhero/>
    </div>
    {/* end of superhero */}
    </div>
  );
}

export default App;
