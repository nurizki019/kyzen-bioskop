import {Row, Col, Container, Button} from "react-bootstrap"
const Intro = () => {
    return (
        <div className="intro">
        <Container className="text-white d-flex text-center justify-content-center align-items-center">
          <Row>
            <Col>
              <div className="title">WELCOME TO </div>
              <div className="title">BIOSKOP</div>
              <div className="introButton mt-4 text-center">
                <Button href="https://gitlab.com/nurizki019" variant="dark">List Semua Films</Button>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    )
}

export default Intro